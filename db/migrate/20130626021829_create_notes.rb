class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :subject_id
      t.string :title
      t.text :memo
      t.datetime :datetime

      t.timestamps
    end
  end
end
