json.array!(@subjects) do |subject|
  json.extract! subject, :title, :memo
  json.url subject_url(subject, format: :json)
end
