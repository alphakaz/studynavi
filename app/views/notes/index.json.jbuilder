json.array!(@notes) do |note|
  json.extract! note, :subject_id, :title, :memo, :date
  json.url note_url(note, format: :json)
end
